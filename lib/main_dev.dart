import 'package:flutter/material.dart';
import 'package:flutter_flavors/resources/app_config.dart';
import 'package:flutter_flavors/main.dart';

void main() {
  var configuredApp = AppConfig(
    appTitle: 'Flutter Flavors Dev',
    buildFlavot: "Development",
    child: MyApp(),
  );
  return runApp(configuredApp);
}
