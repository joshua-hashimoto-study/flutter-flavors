import 'package:flutter/material.dart';
import 'package:flutter_flavors/resources/app_config.dart';
import 'package:flutter_flavors/ui/pages/home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppConfig.of(context).appTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}
