import 'package:flutter/material.dart';
import 'package:flutter_flavors/resources/app_config.dart';
import 'package:flutter_flavors/main.dart';

void main() {
  var configuredApp = AppConfig(
    appTitle: 'Flutter Flavors',
    buildFlavot: "Production",
    child: MyApp(),
  );
  return runApp(configuredApp);
}
